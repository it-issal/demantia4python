from code2use.core.abstract import *

################################################################################

@Ontology2use.register('user', label="User Profile")
class UserProfile(Ontology2use):
    def initialize(self):
        pass

################################################################################

@Ontology2use.register('place', label="Geo Place")
class PlaceProfile(Ontology2use):
    def initialize(self):
        pass

################################################################################

@Ontology2use.register('brand', label="Social Brand")
class BrandProfile(Ontology2use):
    def initialize(self):
        pass

    def declare(self):
        pass
