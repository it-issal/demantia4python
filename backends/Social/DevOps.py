from code2use.core.abstract import *

from github import GitHub

################################################################################

@Social2use.register('github', icon='github', title="Github")
class GithubAPI(Social2use):
    def initialize(self):
        self.cnx = GitHub(access_token=self.vault['access_token'])

    def invoke(self, path, **params):
        pass
    
    @property
    def repos(self):
        return self.cnx.users('shivha').repos().get()

#*******************************************************************************

@Social2use.register.mapping('user', 'github', title="Github User")
class GithubUser(Social2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Cloud2use.Resource):
        pass

#*******************************************************************************

@Social2use.register.mapping('repo', 'github', title="Github Repository")
class GithubRepo(Social2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Cloud2use.Resource):
        pass
