from code2use.core.abstract import *

################################################################################

@Social2use.register('facebook', icon='facebook', title="Facebook")
class FacebookAPI(Social2use):
    def initialize(self, *args, **kwargs):
        pass

    def invoke(self, path, **params):
        pass

#*******************************************************************************

@Social2use.register.mapping('user', 'facebook', title="Facebook User")
class FacebookUser(Social2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Social2use.Resource):
        pass

################################################################################

@Social2use.register('twitter', icon='twitter', title="Twitter")
class TwitterAPI(Social2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

#*******************************************************************************

@Social2use.register.mapping('user', 'twitter', title="Twitter User")
class TwitterUser(Social2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Social2use.Resource):
        pass

################################################################################

@Social2use.register('foursquare', icon='foursquare', title="4square")
class FoursquareAPI(Social2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

#*******************************************************************************

@Social2use.register.mapping('user', 'foursquare', title="Foursquare User")
class FoursquareUser(Social2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Social2use.Resource):
        pass
