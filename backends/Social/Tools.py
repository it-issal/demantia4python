from code2use.core.abstract import *

################################################################################

@Social2use.register('trello', icon='trello', title="Trello")
class TrelloAPI(Social2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

#*******************************************************************************

@Social2use.register.mapping('user', 'trello', title="Trello User")
class TrelloUser(Social2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Cloud2use.Resource):
        pass

#*******************************************************************************

@Social2use.register.mapping('repo', 'trello', title="Trello Repository")
class TrelloRepo(Social2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Cloud2use.Resource):
        pass
