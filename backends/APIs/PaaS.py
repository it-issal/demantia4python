from code2use.core.abstract import *

################################################################################

@Cloud2use.register('heroku', icon='cubes', title="Heroku")
class HerokuAPI(Cloud2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

#*******************************************************************************

@Cloud2use.register.mapping('heroku', 'application', title="Heroku application")
class HerokuApp(Cloud2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Cloud2use.Resource):
        pass

################################################################################

@Cloud2use.register('openshift', icon='gears', title="OpenShift")
class OpenShiftAPI(Cloud2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

#*******************************************************************************

@Cloud2use.register.mapping('openshift', 'application', title="OpenShift application")
class OpenShiftApp(Cloud2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Cloud2use.Resource):
        pass
