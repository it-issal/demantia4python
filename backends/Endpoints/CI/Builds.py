from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('jenkins', icon='industry', title="Jenkins C.I")
class JenkinsAPI(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'jenkins', title="Jenkins User")
class JenkinsUser(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
