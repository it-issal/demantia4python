from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('gitlab', icon='github', title="Gitlab Server")
class GitlabAPI(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'gitlab', title="Gitlab User")
class GitlabUser(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass

################################################################################

import code2use.backends.Endpoints.CI.Builds
import code2use.backends.Endpoints.CI.Integration
