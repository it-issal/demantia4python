from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('strider', icon='refresh', title="Strider-CD")
class StriderAPI(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'strider', title="Strider-CD User")
class StriderUser(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
