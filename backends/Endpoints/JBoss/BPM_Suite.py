from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('jboss-bpms', icon='cogs', title="JBoss BPM Suite")
class JBossBPMs_API(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'process', title="JBoss BPM Suite User")
class JBossBPMs_User(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
