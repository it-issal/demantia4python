from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('jboss-portal', icon='globe', title="JBoss Portal")
class JBossPortal_API(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'jboss-portal', title="JBoss Portal User")
class JBossPortal_User(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
