from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('jboss-brms', icon='building', title="JBoss BRMS")
class JBossBRMS_API(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'jboss-brms', title="JBoss BRMS User")
class JBossBRMS_User(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
