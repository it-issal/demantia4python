from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('jboss-dv', icon='database', title="JBoss DataVirt")
class JBossDV_API(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'jboss-dv', title="JBoss DataVirt User")
class JBossDV_User(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
