from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('jboss-fuse', icon='bus', title="JBoss Fuse ESB")
class JBossFuse_API(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'jboss-fuse', title="JBoss Fuse User")
class JBossFuse_User(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
