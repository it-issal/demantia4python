from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('docker', icon='globe', title="Docker")
class DockerAPI(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('container', 'docker', title="JBoss Portal User")
class DockerContainer(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
