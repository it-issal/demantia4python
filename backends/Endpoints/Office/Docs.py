from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('openkm', icon='archive', title="OpenKM")
class OpenkmAPI(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'openkm', title="OpenKM User")
class OpenkmUser(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
