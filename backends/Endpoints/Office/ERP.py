from code2use.core.abstract import *

################################################################################

@Endpoint2use.register('odoo', icon='bank', title="Odoo ERP")
class OdooAPI(Endpoint2use):
    def initialize(self):
        pass

    def invoke(self, path, **params):
        pass

################################################################################

@Endpoint2use.register.mapping('user', 'odoo', title="Odoo User")
class OdooUser(Endpoint2use.Manager):
    def initialize(self):
        pass

    class Wrapper(Endpoint2use.Resource):
        pass
