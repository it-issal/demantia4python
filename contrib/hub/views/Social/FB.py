#-*- coding: utf-8 -*-

from demantia.contrib.hub.shortcuts import *

################################################################################

class Endpoint(object):
    @classmethod
    def register(cls, *args, **kwargs):
        def do_apply(handler, alias, **opts):
            if not hasattr(cls, 'REGISTRY'):
                setattr(cls, 'REGISTRY', {})

            if alias not in cls.REGISTRY:
                cls.REGISTRY[alias] = handler

            return handler

        return lambda handler: do_apply(handler, *args, **kwargs)

    #***************************************************************************

    def __init__(self, *args, **kwargs):
        self.initialize(*args, **kwargs)

    ontologies = property(lambda self: self.REGISTRY)

    #***************************************************************************

    def resolve(self, ontology, **narrow):
        if ontology in self.REGISTRY:
            handler = self.REGISTRY[ontology]

            if hasattr(handler, 'resolve'):
                return handler.resolve(self, narrow)
            else:
                return None
        else:
            raise OntologyDoesNotExist(self, ontology, narrow)

    def query(self, ontology, **narrow):
        if ontology in self.REGISTRY:
            handler = self.REGISTRY[ontology]

            resp = []

            for entry in handler.query(self, narrow):
                if hasattr(handler, 'extract'):
                    entry = handler.extract(entry)

                item = handler(self, ontology, entry)

                resp.append(item)

            return resp
        else:
            raise OntologyDoesNotExist(self, ontology, narrow)

#*******************************************************************************

class SocialEndpoint(Endpoint):
    def initialize(self, request, **config):
        self._req   = request

        self._cfg   = config

        self.prepare()

        self.reset()

    def reset(self):
        self._prx   = self.request.user.social_auth.get(provider=self.PROVIDER)

        self._cnx   = self.connect(self.creds)

    request = property(lambda self: self._req)
    config  = property(lambda self: self._config)

    proxy   = property(lambda self: self._prx)
    conn    = property(lambda self: self._cnx)

    #***************************************************************************

    user    = property(lambda self: self.request.user)
    creds   = property(lambda self: self.proxy.extra_data)

#*******************************************************************************

class SocialObject(object):
    def __init__(self, endpoint, raw):
        self._ep  = endpoint
        self._raw = raw

    endpoint = property(lambda self: self._ep)
    raw_data = property(lambda self: self._raw)

    def cleanup(self, raw):
        pass

################################################################################

import facebook

#*******************************************************************************

class OpenGraph(SocialEndpoint):
    PROVIDER = 'facebook'

    def prepare(self):
        pass # self.authorize('token', request.social_auth['facebook']['api_token'])

    def connect(self, creds):
        return facebook.GraphAPI(access_token=self.creds['access_token']) #, version='2.2')

    #***************************************************************************

    @property
    def users(self):
        return self.query('user')

    @property
    def pages(self):
        return self.query('page')

#*******************************************************************************

class FacebookObject(SocialObject):
    def narrow(self, raw):
        return raw['id']

    def describe(self, raw):
        return raw

#*******************************************************************************

@OpenGraph.register('user')
class FacebookUser(FacebookObject):
    @classmethod
    def resolve(cls, ogp, narrow):
        raw = ogp.conn.get_object(**narrow)

        return raw

    @classmethod
    def query(cls, ogp, narrow):
        raw = ogp.conn.get_objects(**narrow)

        return raw

    @classmethod
    def extract(cls, raw):
        return raw

    #---------------------------------------------------------------------------

    def insights(self, *fields, **options):
        pass

#*******************************************************************************

@OpenGraph.register('page')
class FacebookPage(FacebookObject):
    @classmethod
    def resolve(cls, ogp, narrow):
        raw = ogp.conn.get_object(**narrow)

        return raw

    @classmethod
    def query(cls, ogp, narrow):
        raw = ogp.conn.get_objects(**narrow)

        return raw

    @classmethod
    def extract(cls, raw):
        return raw

    #---------------------------------------------------------------------------

    @property
    def graph(self):
        if not hasattr(self, '_grp'):
            cnx = facebook.GraphAPI(access_token=self['access_token']) #, version='2.2')

            setattr(self, '_grp', cnx)
        
        return self._grp

    #---------------------------------------------------------------------------

    def insights(self, *fields, **options):
        pass

################################################################################

@fqdn.route(r'^\+facebook/$', strategy='login')
def company_list(context):
    context.template = 'hub/social/fb/overview.html'

    FB = OpenGraph(context)

    return {
        'pages': FB.pages,
    }

################################################################################

@fqdn.route(r'^\+facebook/insights/(?P<narrow>.+)/$', strategy='login')
def page_insights(context, narrow):
    context.template = 'hub/social/fb/insights.html'

    FB = OpenGraph(context)

    page = FB.resolve('page', narrow)

    if page is not None:
        return {
            'page': page,
            'insights': {
                'activity': page.activity,
            },
        }

################################################################################

@fqdn.route(r'^\+facebook/insights/(?P<narrow>.+)/(?P<lookup>.+).json$', strategy='login')
def page_insights_json(context, narrow, lookup):
    FB = OpenGraph(context)

    page = FB.resolve('page', narrow)

    if page is not None:
        return {
            'page': page,
        }

