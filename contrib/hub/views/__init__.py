#-*- coding: utf-8 -*-

from demantia.contrib.hub.shortcuts import *

################################################################################

@fqdn.route(r'^$', strategy='login')
def homepage(context):
    context.template = 'hub/views/homepage.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^plug$', strategy='login')
def plug(context):
    context.template = 'hub/views/plug/landing.html'

    import code2use.backends.APIs
    import code2use.backends.Endpoints

    from code2use.core.abstract import Ontology2use, Cloud2use, Endpoint2use, Social2use

    return {
        'registrars': dict([
            (provider, [
                entry['name']
                for entry in provider.register.all()
            ])
            for provider in (Ontology2use, Cloud2use, Endpoint2use, Social2use)
        ]),
        'apis':      Cloud2use.register.all(),
        'endpoints': Endpoint2use.register.all(),
        'networks':  Social2use.register.all(),
    }

################################################################################

@fqdn.route(r'^plug/api/(?P<provider>.+)$', strategy='login')
def plug_api(context, provider):
    context.template = 'hub/views/plug/endpoint.html'

    from code2use.core.abstract import Ontology2use, Cloud2use, Endpoint2use

    import code2use.backends.APIs

    return {
        'provider': provider,
    }

#*******************************************************************************

@fqdn.route(r'^plug/endpoint/(?P<provider>.+)$', strategy='login')
def plug_endpoint(context, provider):
    context.template = 'hub/views/plug/endpoint.html'

    from code2use.core.abstract import Ontology2use, Cloud2use, Endpoint2use

    import code2use.backends.Endpoints

    return {
        'provider': provider,
    }

################################################################################

from . import Mgmt

from . import Social

################################################################################

urlpatterns = fqdn.urlpatters

