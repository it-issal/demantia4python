from django.contrib import admin

from demantia.contrib.hub.models import *

################################################################################

class SocialAdmin(admin.ModelAdmin):
    list_display = ('provider', 'created_on')
    list_filter  = ('created_on',)

admin.site.register(CloudSocial, SocialAdmin)

#*******************************************************************************

class ApiAdmin(admin.ModelAdmin):
    list_display = ('provider', 'organization', 'created_on')
    list_filter  = ('created_on', 'organization')

admin.site.register(CloudAPI, ApiAdmin)

#*******************************************************************************

class ManagerAdmin(admin.ModelAdmin):
    list_display = ('provider', 'organization', 'alias', 'target_link', 'created_on')
    list_filter  = ('created_on', 'provider', 'organization')

admin.site.register(CloudManager, ManagerAdmin)
