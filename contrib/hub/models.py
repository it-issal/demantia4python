#-*- coding: utf-8 -*-

from .utils import *

#*******************************************************************************

from deming.contrib.console.models import *

################################################################################

class CloudEndpoint(models.Model):
    created_on   = models.DateTimeField(auto_now_add=True)

    @property
    def creds(self):
        try:
            return json.loads(self.api_creds)
        except:
            return {}

    @property
    def config(self):
        try:
            return json.loads(self.api_config)
        except:
            return {}

    @property
    def vault(self):
        try:
            return json.loads(self.api_vault)
        except:
            return {}

    class Meta:
        abstract = True

################################################################################

class CloudSocial(CloudEndpoint):
    provider     = models.CharField(max_length=32, choices={
        'bitbucket': 'BitBucket',
        'github':    'GitHub',
    }.iteritems())

    api_creds    = models.TextField(default='{}')
    api_config   = models.TextField(default='{}')
    api_vault    = models.TextField(default='{}')

################################################################################

class CloudAPI(CloudEndpoint):
    organization = models.ForeignKey(Organization, related_name='apis')

    provider     = models.CharField(max_length=32, choices={
        'heroku':    'Heroku',
        'openshift': 'RedHat OpenShift',
    }.iteritems())

    api_creds    = models.TextField(default='{}')
    api_config   = models.TextField(default='{}')
    api_vault    = models.TextField(default='{}')

#*******************************************************************************

class CloudManager(CloudEndpoint):
    organization = models.ForeignKey(Organization, related_name='endpoints')

    alias        = models.CharField(max_length=128)
    provider     = models.CharField(max_length=32, choices={
        'ci-strider':     'C.I :: Strider CD',
        'ci-jenkins':     'C.I :: Jenkins',
        'ci-gitlab':      'C.I :: GitLab',
        'office-odoo':    'Office :: Odoo',
        'office-openkm':  'Office :: OpenKM',
        'jboss-portal':   'JBoss :: Portal',
        'jboss-datavirt': 'JBoss :: Data Virtualization',
        'jboss-push':     'JBoss :: Universal Push',
        'jboss-fuse':     'JBoss :: Fuse (ESB)',
        'jboss-bpmsuite': 'JBoss :: BPM Suite',
        'jboss-brms':     'JBoss :: BRMS',
        'web-wordpress':  'Web :: WordPress',
        'web-opencart':   'Web :: OpenCart',
    }.iteritems())

    target_link  = models.CharField(max_length=256)
    @property
    def target_ep(self):
        from urlparse import urlparse

        try:
            return urlparse(self.target_link)
        except:
            return urlparse('#')

    api_creds    = models.TextField(default='{}')
    api_config   = models.TextField(default='{}')
    api_vault    = models.TextField(default='{}')
