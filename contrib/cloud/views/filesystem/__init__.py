#-*- coding: utf-8 -*-

from demantia.contrib.cloud.shortcuts import *

#*******************************************************************************

@fqdn.route(r'^fs/$', strategy='login')
def listing(context):
    context.template = 'cloud/rest/fs/list.html'

    return {'listing': ScmRepository.objects()}

################################################################################

@fqdn.route(r'^fs/(?P<name>.+)/$', strategy='login')
def overview(context, name):
    context.template = 'cloud/rest/fs/view.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^fs/(?P<name>.+)/quota$', strategy='login')
def quota(context, name):
    context.template = 'cloud/rest/fs/quota.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^fs/(?P<name>.+)/explorer$', strategy='login')
def explorer_view(context):
    context.template = 'cloud/views/explorer.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^fs/(?P<name>.+)/rpc/$', strategy='login')
def explorer_api(context):
    context.template = 'cloud/views/filesystem.html'

    return {'version': version}
