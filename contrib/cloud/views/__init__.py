#-*- coding: utf-8 -*-

from demantia.contrib.cloud.shortcuts import *

################################################################################

@fqdn.route(r'^$', strategy='login')
def homepage(context):
    context.template = 'cloud/views/homepage.html'

    return {'version': version}

################################################################################

from . import nestrix

#*******************************************************************************

urlpatterns = fqdn.urlpatters
