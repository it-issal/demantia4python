#-*- coding: utf-8 -*-

from demantia.contrib.cloud.shortcuts import *

################################################################################

@fqdn.route(r'^nestrix/routing/?$', strategy='login')
def listing(context):
    context.template = 'cloud/views/nestrix/route/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^nestrix/routing/(?P<provider>.+)/(?P<owner>.+)/(?P<slug>.+)$', strategy='login')
def overview(context, provider, owner, slug):
    context.template = 'cloud/views/nestrix/route/view.html'

    return {'version': version}
