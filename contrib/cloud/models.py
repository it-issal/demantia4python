#-*- coding: utf-8 -*-

from .utils import *

########################################################################

class FS_Provider(models.Model):
	name    = models.CharField(max_length=48)
	title   = models.CharField(max_length=128, blank=True)

	handler = models.CharField(max_length=128)
	params  = models.TextField(default='{}')

	class Meta:
		verbose_name        = "FS provider"
		verbose_name_plural = "FS providers"

#***********************************************************************

class FS_Endpoint(models.Model):
	owner    = models.ForeignKey(CustomUser, related_name='endpoints')
	name     = models.CharField(max_length=48)

	link     = models.CharField(max_length=256)
	provider = models.ForeignKey(FS_Provider, related_name="endpoints", blank=True, null=True, default=None)

	title    = models.CharField(max_length=128, blank=True)
	config   = models.TextField(default='{}')

	def save(self, *args, **kwargs):
		qs = FS_Provider.objects.filter(urlparse(self.link).scheme)

		if len(qs):
			self.provider = qs[0]

		return super(FS_Endpoint, self).save(*args, **kwargs)

	class Meta:
		verbose_name        = "FS endpoint"
		verbose_name_plural = "FS endpoints"
