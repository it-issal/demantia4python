from django.contrib import admin

from demantia.contrib.cloud.models import *

################################################################################

class ProviderAdmin(admin.ModelAdmin):
    list_display = ('name', 'title', 'handler', 'params')

admin.site.register(FS_Provider, ProviderAdmin)

#*******************************************************************************

class EndpointAdmin(admin.ModelAdmin):
    list_display = ('owner', 'name', 'title', 'link', 'provider')
    list_filter  = ('provider__name', 'owner__username')

admin.site.register(FS_Endpoint, EndpointAdmin)
