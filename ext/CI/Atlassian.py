#-*- coding: utf-8 -*-

from demantia.abstract import *

from bitbucket import bitbucket

@ExtManager.register('bitbucket')
class BitBucket_Deming(DemingExt):
    def initialize(self):
        self._cnx = bitbucket.Bitbucket(self.cfg['user'], self.cfg['passwd'], repo_name_or_slug='homeless')

