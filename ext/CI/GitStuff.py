#-*- coding: utf-8 -*-

from demantia.shortcuts import *

################################################################################

import github

@ExtManager.register('github')
class Github_Deming(DemingExt):
    def initialize(self):
        self._cnx = github.GitHub(self.cfg['user'], self.cfg['passwd'])

################################################################################

import gitlab

@ExtManager.register('gitlab')
class Gitlab_Deming(DemingExt):
    def initialize(self):
        self._cnx = gitlab.Gitlab("https://" + self.cfg.get('passwd', "gitlab.com"),
            email    = self.cfg['user'],
            password = self.cfg['passwd'],
        )

        self._glb.auth()
